package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

//creation of property transfer smart contract
type PropertyTransferSmartContract struct {
	contractapi.Contract
}

//Define property object
type Property struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Area      int    `json:"area"`
	OwnerName string `json:"ownerName"`
	Value     int    `json:"value"`
}

//Add Property to ledger
//smart contract instance as reciever(golang class kinda)
func (pc *PropertyTransferSmartContract) AddProperty(ctx contractapi.TransactionContextInterface,
	id string, name string, area int, ownerName string, value int) error {
	propertyJSON, err := ctx.GetStub().GetState(id) //checks if id already exists
	if err != nil {
		return fmt.Errorf("Failed to read the data from world state", err)
	}
	if propertyJSON != nil {
		return fmt.Errorf("The property %s already exists", id)
	}

	prop := Property{ //create object
		ID:        id,
		Name:      name,
		Area:      area,
		OwnerName: ownerName,
		Value:     value,
	}

	propertyBytes, err := json.Marshal(prop) //create json encoding
	if err != nil {
		return err
	}
	return ctx.GetStub().PutState(id, propertyBytes) //pass json to api

}

//Query All existing properties
func (pc *PropertyTransferSmartContract) QueryAllProperties(ctx contractapi.TransactionContextInterface) ([]*Property, error) {
	//Range with blank values will return all values
	propertyIterator, err := ctx.GetStub().GetStateByRange("", "")
	if err != nil {
		return nil, err
	}
	defer propertyIterator.Close()
	//return value definition
	var properties []*Property
	for propertyIterator.HasNext() {
		//iterate through each property that was returned
		propertyResponse, err := propertyIterator.Next()
		if err != nil {
			return nil, err
		}
		//convert each from JSON and append to return val
		var property *Property
		err = json.Unmarshal(propertyResponse.Value, &property)
		if err != nil {
			return nil, err
		}
		properties = append(properties, property)
	}
	return properties, nil
}

//Query by ID
func (pc *PropertyTransferSmartContract) QueryPropertyByID(ctx contractapi.TransactionContextInterface, id string) (*Property, error) {
	//get property by ID
	propertyJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("Failed to read the data from world state", err)
	}
	if propertyJSON == nil {
		return nil, fmt.Errorf("the property %s does not exist", id)
	}
	//convert json to golang obj
	var property *Property
	err = json.Unmarshal(propertyJSON, &property)
	if err != nil {
		return nil, err
	}
	return property, nil
}

//Transfer property ownership
func (pc *PropertyTransferSmartContract) TransferProperty(ctx contractapi.TransactionContextInterface,
	id string, newOwner string) error {
	//get current property information
	property, err := pc.QueryPropertyByID(ctx, id)
	if err != nil {
		return err
	}
	//update property ownership
	property.OwnerName = newOwner
	propertyJSON, err := json.Marshal(property)
	if err != nil {
		return err
	}
	//return updated property ownership
	return ctx.GetStub().PutState(id, propertyJSON)
}

func main() {
	//new instance of smart contract struct
	propTransferSmartContract := new(PropertyTransferSmartContract)
	//Add chain code
	cc, err := contractapi.NewChaincode(propTransferSmartContract)
	if err != nil {
		panic(err.Error())
	}
	//Init chain code
	if err := cc.Start(); err != nil {
		panic(err.Error())
	}
}
